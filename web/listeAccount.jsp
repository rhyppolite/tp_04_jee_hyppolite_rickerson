<%--
    Document   : index
    Created on : 16 sept. 2009, 16:54:32
    Author     : michel buffa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!-- Ne pas oublier cette ligne sinon tous les tags de la JSTL seront ignorés ! -->
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Comptes Bancaires</title>
        <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <h3>Gestionnaire de comptes</h3>


            <!-- Message qui s'affiche lorsque la page est appelé avec un paramètre http message -->
            <c:if test="${!empty param['message']}">
                <h4>Reçu message : ${param.message}</h4>
            </c:if>

                <li><a href="ServletComptes?action=creerUnCompte">Créer un compte</a></li>
                
                <c:if test="${param['action'] == 'listerComptes'}" >
                <h2>Liste des comptes bancaires</h2>

                <table class="table">
                    <!-- La ligne de titre du tableau des comptes -->


                    <thead>
                        <tr>    
                            <th>Prénom</th>
                            <th>Nom</th>
                            <th>Numero compte</th>
                            <th>Balance</th>
                        </tr>
                    </thead>

                    <!-- Ici on affiche les lignes, une par utilisateur -->
                    <!-- cette variable montre comment on peut utiliser JSTL et EL pour calculer -->
                    <c:set var="total" value="0"/>

                    <c:forEach var="u" items="${requestScope['listeDesComptes']}">
                        <tbody>
                            <tr>
                                <td>${u.firstName}</td>
                                <td>${u.lastName}</td>
                                <td>${u.accountNumber}</td>
                                <td>${u.balance}</td>
                                <!-- On compte le nombre de users -->
                                <c:set var="total" value="${total+1}"/>
                            </tr>
                        </tbody>
                    </c:forEach>

                    <!-- Affichage du solde total dans la dernière ligne du tableau -->
                    <tr><td><b>TOTAL</b></td><td></td><td><b>${total}</b></td><td></td></tr>
                </table>

            </c:if>


        </div>   
    </body>
</html>
