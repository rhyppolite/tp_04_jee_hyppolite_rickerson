<%--
    Document   : index
    Created on : 16 sept. 2009, 16:54:32
    Author     : michel buffa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!-- Ne pas oublier cette ligne sinon tous les tags de la JSTL seront ignorés ! -->
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Comptes Bancaires</title>
        <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <h3>Gestionnaire de comptes</h3>


            <!-- Message qui s'affiche lorsque la page est appelé avec un paramètre http message -->
            <c:if test="${!empty param['message']}">
                <h4>Reçu message : ${param.message}</h4>
            </c:if>

                <li><a href="ServletComptes?action=listerComptes">Liste des comptes</a></li>
            
            <ol class="list-group list-group-numbered">
                <li ><strong>Créer un compte</strong></li>

                
                <form action="ServletComptes" method="post">

                    <div class="form-group">
                        <label for="nom">Nom</label>
                        <input type="text"  class="form-control" name="nom" required/>

                    </div>
                    <div class="form-group">
                        <label for="prenom">Prenom</label>
                        <input type="text" class="form-control" name="prenom" required/>

                    </div>
                    <div class="form-group">
                        <label for="login">Numero Compte</label>
                        <input type="text" class="form-control" name="compte" required/>

                    </div>
                    
                    <div class="form-group">
                        <label for="login">Montant</label>
                        <input type="text" class="form-control" name="balance" required/>

                    </div>
                    <!-- Astuce pour passer des paramètres à une servlet depuis un formulaire JSP !-->
                    <input type="hidden" name="action" value="creerUnCompte"/>
                    <button type="submit" class="btn btn-success">Créer le compte</button><br><br>

                </form>

        </div>      
    </body>
</html>
