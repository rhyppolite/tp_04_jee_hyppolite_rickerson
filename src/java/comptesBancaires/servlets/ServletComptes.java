/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comptesBancaires.servlets;

import comptesBancaires.modeles.CompteBancaire;
import comptesBancaires.sessions.CompteBancaireFacade;
import java.io.IOException;
import java.util.Collection;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class ServletComptes extends HttpServlet {

    @EJB
    private CompteBancaireFacade compteBancaireFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * 
     * 
     */
    
    String nom = null;
    String prenom = null;
    String compte = null;
    Double montant;
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String forwardTo = "";
        String message = "";

        if (action != null) {
            if (action.equals("listerComptes")) {
                Collection<CompteBancaire> liste = compteBancaireFacade.getAllAccount();
                request.setAttribute("listeDesComptes", liste);
                forwardTo = "listeAccount.jsp?action=listerComptes";
                message = "Liste des comptes";
            } else {
                forwardTo = "newAccount.jsp?action=todo";
                message = "Bonjour";
            }
        }
        RequestDispatcher dp = request.getRequestDispatcher(forwardTo + "&message=" + message);
        dp.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        nom = request.getParameter("nom");
        prenom = request.getParameter("prenom");
        compte = request.getParameter("compte");
        montant = Double.parseDouble(request.getParameter("balance"));
        compteBancaireFacade.creerCompte(prenom, nom, compte, montant);

        RequestDispatcher dp = request.getRequestDispatcher("newAccount.jsp?action=todo" + "&message=" + "+ Compte créé avec succès");
        dp.forward(request, response);
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
