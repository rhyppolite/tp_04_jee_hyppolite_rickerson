/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comptesBancaires.sessions;

import comptesBancaires.modeles.CompteBancaire;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Administrator
 */
@Stateless
public class CompteBancaireFacade extends AbstractFacade<CompteBancaire> {

    @PersistenceContext(unitName = "TP_04_JEE_HYPPOLITE_RICKERSONPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CompteBancaireFacade() {
        super(CompteBancaire.class);
    }
    
    public CompteBancaire creerCompte (String firstName, String lastName, String accountNumber, Double balance){
        CompteBancaire cb = new CompteBancaire(firstName,  lastName,  accountNumber,  balance);
        em.persist(cb);
        return cb;
        
    }
    
    
    public Collection<CompteBancaire> getAllAccount() {
        // Exécution d'une requête équivalente à un select *
        Query q = em.createQuery("select ac from CompteBancaire ac");
        return q.getResultList();
    }

}
